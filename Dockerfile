# specify a suitable source image
# EDIT 3: added FROM node info

FROM node:12

WORKDIR /index

COPY package*.json ./

RUN npm ci

# copy the application source code files
# EDIT 4: added COPY info

COPY . . 

EXPOSE 3000

# specify the command which runs the application
# EDIT 5: added CMD info

CMD ["npm", "start"]
